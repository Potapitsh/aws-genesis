resource "aws_eip" "nat-gateway-eip" {
  count = length(aws_subnet.private.*.id)
  vpc   = true
}

resource "aws_nat_gateway" "private" {
  count         = length(aws_subnet.private.*.id)
  allocation_id = aws_eip.nat-gateway-eip.*.id[count.index]
  subnet_id     = aws_subnet.public.*.id[count.index]
}

resource "aws_route_table" "private" {
  count  = length(aws_subnet.private.*.id)
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.vpc_name} private ${count.index}"
  }
}

resource "aws_route" "private" {
  count                  = length(aws_subnet.private.*.id)
  route_table_id         = aws_route_table.private.*.id[count.index]
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.private.*.id[count.index]
}

resource "aws_route_table_association" "private" {
  count          = length(aws_subnet.private.*.id)
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.private.*.id[count.index]
}