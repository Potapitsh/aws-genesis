terraform {
  backend "s3" {
    bucket = "potas-playground-genesis"
    key    = "genesis.tfstate"
    region = "eu-central-1"
  }
}