resource "aws_subnet" "private" {
  count             = var.limit_subnets == -1 ? length(data.aws_availability_zones.available.names) : var.limit_subnets
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 4, count.index + 1)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "${var.vpc_name}-private-${count.index}"
  }
}

resource "aws_subnet" "public" {
  count             = var.limit_subnets == -1 ? length(data.aws_availability_zones.available.names) : var.limit_subnets
  vpc_id            = aws_vpc.main.id
  cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 4, count.index + 10)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = "${var.vpc_name}-public-${count.index}"
  }
}