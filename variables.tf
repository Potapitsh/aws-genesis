variable "region" {
  default = "eu-central-1"
}

variable "limit_subnets" {
  default = -1
}


variable "vpc_name" {}

variable "vpc_cidr_block" {}
