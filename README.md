# aws-genesis

- creates a vpc with private and public subnet groups in each AZ.
- every public subnet gets a internet gateway
- every private subnet gets a nag gateway and an associated EIP
- creates a dynamodb and s3 vpc endpoint ([explanation why to use those]([https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/vpc-endpoints-dynamodb.html))

## usage
```terraform
terraform init
terraform workspace new <main>                      # <main> is jsut a placeholder
terraform plan -out .plan -var-file <main.tfvars>   # as well here
terraform apply .plan
```